from os import getenv
from dotenv import load_dotenv

load_dotenv()
que = {}
SESSION_NAME = getenv("SESSION_NAME", "session")
BOT_TOKEN = getenv("BOT_TOKEN")
BOT_NAME = getenv("BOT_NAME")
BG_IMAGE = getenv("BG_IMAGE", "https://telegra.ph/file/f9c4f6fe3ee7dbef047f7.png")
THUMB_IMG = getenv("THUMB_IMG", "https://telegra.ph/file/c84b55ca515cb68b1d21a.png")
AUD_IMG = getenv("AUD_IMG", "https://telegra.ph/file/2b0709c07ddbee75c7133.png")
QUE_IMG = getenv("QUE_IMG", "https://telegra.ph/file/c84b55ca515cb68b1d21a.png")
admins = {}
API_ID = int(getenv("API_ID"))
API_HASH = getenv("API_HASH")
BOT_USERNAME = getenv("BOT_USERNAME")
ASSISTANT_NAME = getenv("ASSISTANT_NAME")

DURATION_LIMIT = int(getenv("DURATION_LIMIT", "60"))

COMMAND_PREFIXES = list(getenv("COMMAND_PREFIXES", "/ ! .").split())

SUDO_USERS = list(map(int, getenv("SUDO_USERS").split()))
